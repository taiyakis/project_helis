<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ALL USERS

Route::get('/', 'FeedController@viewFeeds');
Route::get('/login', 'UserController@showLogin');
Route::get('/provider/{id}', 'FeedController@providerLink');
Route::get('/feed/{id}', 'FeedController@readfeed');
Route::get('/logout', 'UserController@logout');
Route::post('/filter', 'FeedController@filter');
Route::post('/login', 'UserController@login');

// ADMIN 

Route::get('/crud', 'AdminFeedController@dashboard');
Route::get('/changepassword', 'AdminFeedController@changePass');
Route::post('/changepassword', 'UserController@changepassword');
Route::post('/addcategory', 'UserController@createCategory');
Route::post('/addfeed', 'AdminFeedController@addFeed');
Route::post('/deletefeed', 'AdminFeedController@deleteFeed');
Route::post('/editfeed', 'AdminFeedController@editFeed');