@extends('layouts.home')

@section('head')
<title>Helis | RSS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="wclassth=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}"/>
    <script type="text/javascript" src="/js/alerts.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
@endsection

@push('content')
    <div>
        <h3>
            <span>{{ $feed->header }}</span>
        </h3>
        <div>
                <p>{{ $feed->info }}</p>
        </div>
     </div>
@endpush