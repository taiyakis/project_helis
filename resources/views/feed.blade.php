@extends('layouts.home')

@section('head')
<title>Helis | RSS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="wclassth=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}"/>
    <script type="text/javascript" src="/js/app.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
@endsection


@push('content')
    {{-- Kategorijos --}}
    <div id="feed">
        <form method="POST" action="/filter">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label>Filter by category</label>
                <select name="category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category->category }}">{{ $category->category }}</option>
                    @endforeach
                </select>
            </div>
        <button class="btn btn-primary" type="submit">Submit</button>
        </form>
    </div>
@endpush

@if(count($feeds) == 0)
    @section('nofeeds')
        <p>No Feeds found</p>
    @endsection
@else
    @foreach($feeds as $feed)
        @push('content')
            <div class="feed">
                <h3>
                    <a data-target="{{ "#feedmodal" . $feed->ID }}" data-toggle="modal" target="_blank">
                        {{ $feed->header }}
                    </a>
                </h3>
            
                <div>
                    <h5>{{ $feed->created_at }}
                    <a href="{{ url('/') . "/provider/" . $feed->provider }}" target="_blank">
                        <span>Provider</span>
                    </a>
                    </h5>
                </div>
            </div>
        @endpush

        @push('modal')
            <div class="modal face" id="{{ "feedmodal" . $feed->ID }}" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">{{ $feed->header }}</h4>
                        </div>
                        <div class="modal-body">
                            {{ $feed->info }}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <a href="{{ url('/') . "/feed/" . $feed->ID }}" target="_blank">
                                <button type="button" class="btn btn-primary">Open feed</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endpush
    @endforeach
@endif