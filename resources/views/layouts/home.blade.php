<!DOCTYPE html>

<html lang="en">
<head>
    @yield('head')
</head>
<body>
    <div class="container">
        @stack('content')
        @stack('modal')
        @yield('nofeeds')
    </div>
</body>
</html>