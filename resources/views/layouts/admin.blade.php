<!DOCTYPE html>
<html lang="en">
<head>
    @yield('head')
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="navbar-brand">Admin Dashboard</div>
            </div>
            @yield('logout')
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/crud">Dashboard</a></li>
                <li><a href="/changepassword">Settings</a></li>
            </ul>
        </div>
    </nav>

    <div class="container">
        @yield('content')
        @stack('content')
    </div>
</body>
</html>