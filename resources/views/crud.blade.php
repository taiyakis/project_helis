@extends('layouts.admin')

@section('head')
<title>Helis | RSS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="wclassth=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}"/>
    <script type="text/javascript" src="/js/toggle-edit.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
@endsection

@section('logout')
<ul class="nav navbar-nav navbar-right">
   <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
</ul>
@endsection


@if(empty($feeds))
    <p>No Feeds found</p>
@else
    @push('content')
        <div class="formfix">
            <form method="POST" action="/addfeed">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>Header</label>
                    <input class="form-control" type="text" name="header">
                </div>
                <div class="form-group">
                    <label>Feed content</label>
                    <textarea class="form-control" type="text" name="info"></textarea>
                </div>
                    
                <div class="form-group">
                    <label>Category</label>
                    <select name="category" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->category }}">{{ $category->category }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Provider</label>
                    <input class="form-control" type="text" name="provider">
                </div>
                <button class="btn btn-primary right" type="submit">Submit</button>
            </form>
        </div>
    @endpush

    @push('content')
        <h1>Feeds</h1>
    @endpush

    @foreach($feeds as $feed)
        @push('content')
            <div class="crud">
                <div class="crudfeed">
                    <div class="left">
                        <h3><span>{{ $feed->header }}</span></h3>
                        <h6>{{ $feed->created_at . "  " . $feed->provider }}</h6>
                    </div>
                    <div class="right">
                        <form method="POST" action="/deletefeed">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $feed->ID }}">
                            <button class="btn btn-danger" type="submit" >Delete</button>
                        </form>
                    </div>

                    <div class="right gap">
                        <div class="edit">
                            <button class="btn btn-success" onclick="toggle_hide('{{ "edit" . $feed->ID }}')" type="submit">EDIT</button>
                        </div>
                    </div>
                </div>

                <div class="hidden formfix" id="{{ "edit" . $feed->ID }}">
                    <form method="POST" action="/editfeed">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $feed->ID }}">
                        <div class="form-group">
                            <label>Header</label>
                            <input class="form-control" type="text" name="header" value="{{ $feed->header }}">
                        </div>
                        <div class="form-group">
                            <label>Feed content</label>
                            <textarea class="form-control"  type="text" name="info">{{ $feed->info }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Provider</label>
                            <input class="form-control" type="text" name="provider" value="{{ $feed->provider }}">
                        </div>
                        <div class="form-group">
                            <label>URL</label>
                            <input class="form-control" type="text" name="url" value="{{ $feed->ID }}">
                        </div>
                        <div class="form-group">
                            <select name="category" class="form-control">
                                @foreach($categories as $category)
                                    @if($category->category == $feed->category)
                                        <option value="{{ $category->category }}" selected="selected">{{ $category->category }}</option>
                                    @else
                                        <option value="{{ $category->category }}">{{ $category->category }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary right" type="submit">Update</button>
                    </form>
                </div>   
            </div>
        @endpush
    @endforeach
@endif