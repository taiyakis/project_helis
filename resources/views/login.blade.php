    @extends('layouts.admin')

    @section('head')
    <title>Helis | RSS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="wclassth=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/styles.css')}}"/>
        <script type="text/javascript" src="/js/app.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @endsection

    @section('content')
        <div class="login">
            <form method="POST" action="/login">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                
                <div class="form-group">
                    <label>Username</label>
                    <input class="form-control" name="username">
                </div>
                
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control"name="password">
                </div>
                <button type="submit" class="btn btn-primary right">Log in</button>
            </form>
        </div>
    @endsection