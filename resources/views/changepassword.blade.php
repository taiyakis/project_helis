@extends('layouts.admin')

    @section('head')
    <title>Helis | RSS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="wclassth=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/styles.css')}}"/>
        <script type="text/javascript" src="/js/alerts.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @endsection

    @section('logout')
    <ul class="nav navbar-nav navbar-right">
        <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
    @endsection

    @section('content')
    <div class="settings">
        <div class="formfix">
            <form method="POST" action="/changepassword">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <label>Old password</label>
                <div class="form-group">
                    <input class="form-control" name="oldpass">
                </div>
                <label>New password</label>
                <div class="form-group">
                    <input class="form-control"name="newpass1">
                </div>
                <label>Reenter password</label>
                <div class="form-group">
                    <input class="form-control"name="newpass2">
                </div>
                <button type="submit" class="btn btn-primary right">Submit</button>
            </form>
        </div>

        <div class="formfix">
            <form method="POST" action="/addcategory">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <label>Category</label>
                <div class="form-group">
                    <input class="form-control" name="category">
                </div>
                <button type="submit" class="btn btn-primary right">Submit</button>
            </form>
        </div>
    </div>
    @endsection