1) php artisan user:create [name]

This command creates you an account with random generated password which you can change later in website.
Important: Dont close CMD or you will lose your generated password, but if you can remember it ignore this :)

2) php artisan feed:update

This command used to approve all newly added feeds

3) Import helis.sql file to your database 

sql file will be located in root folder, also dont forget to update .env file

4) To run laravel project type: "php artisan serve" in your CMD


To access admin panel in website, use www.example.lt/login

-- Admin links --

www.example.lt/login

www.example.lt/crud		[ Here you can add, update, delete all feeds before they are approved ]

-----------------

