function toggle_hide(id) {
    var form = document.getElementById(id);
    if ( form.classList.contains('hidden')) {
        form.classList.remove('hidden');
    } else {
        form.classList.add('hidden');
    }
}