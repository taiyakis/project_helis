<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\User;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$getMessage = DB::table('users')->where(['name'=>$this->argument('user')])->first();

        $getMessage = User::where('name', $this->argument('user'))->first();

        if(count($getMessage) == 0)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 6; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $newUser = new User;
            $newUser->name = $this->argument('user');
            $newUser->password = Hash::make($randomString);
            $newUser->save();

            $this->info("Admin user created Succefully\nUsername: " . $this->argument('user') . "\npassword: " . $randomString);
        }
        else
        {
            $this->info("User already exist");
        }
    }
}
