<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Feed;
use App\Category;

class FeedController extends Controller
{
    public function viewFeeds()
    {
        $feeds = Feed::where('approved', '1')->orderBy('created_at', 'desc')->get();
        $categories = Category::get();
        return view('feed', compact('feeds', 'categories'));
    }

    public function filter(Request $request)
    {
        $filter = $request->get('category');
        
        $feeds = Feed::where('category', $filter)->orderBy('created_at', 'desc')->get();
        $categories = Category::get();
        return view('feed', compact('feeds', 'categories'));
    }

    public function readFeed($id)
    {
        $feed = Feed::where('ID', $id)->first();

        if(!empty($feed))
        {
            return view('readfeed', compact('feed'));
        }
        return abort(404);
    }

    public function providerLink($provider)
    {
        return redirect('http://' . $provider);
    }
}
