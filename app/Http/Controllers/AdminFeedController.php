<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Feed;
use App\Category;
use Session;

class AdminFeedController extends Controller
{
    public function dashboard()
    {
        if(!empty(session('name')))
        {
            $feeds = Feed::orderBy('created_at', 'desc')->get();
            $categories = Category::get();
            return view('crud', compact('feeds', 'categories'));
        }
        return redirect('/');
    }

    public function addFeed(Request $request)
    {
        if(!empty(session('name')))
        {
            $header = $request->get('header');
            $info = $request->get('info');
            $category = $request->get('category');
            $provider = $request->get('provider');

            $newFeed = new Feed();
            $newFeed->header = $header;
            $newFeed->provider = $provider;
            $newFeed->info = $info;
            $newFeed->category = $category;
            $newFeed->approved = 0;
            $newFeed->save();

            return redirect ('/crud');
        }
        return redirect('/');
    }

    public function editFeed(Request $request)
    {
        if(!empty(session('name')))
        {
            $header = $request->get('header');
            $info = $request->get('info');
            $category = $request->get('category');
            $url = $request->get('url');
            $provider = $request->get('provider');
            $id = $request->get('id');

            Feed::where('ID', $id)->update(['ID' => $url, 'provider' => $provider, 'header' => $header, 'info' => $info, 'category' => $category]);

            return redirect ('/crud');
        }
        return redirect('/');
    }

    public function deleteFeed(Request $request)
    {
        if(!empty(session('name')))
        {
            $id = $request->get('id');

            Feed::where('ID', $id)->delete();

            return redirect ('/crud');
        }
        return redirect('/');
    }

    public function changePass()
    {
        if(!empty(session('name')))
        {
            return view('changepassword');
        }
        return redirect('/');
    }
}
