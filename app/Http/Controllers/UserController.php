<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Category;
use App\Feed;
use Session;

class UserController extends Controller {

    public function showLogin()
    {
        if (empty(session('name')))
        {
            return view('login');
        }
        else
        {
            return redirect('/crud');
        }
    }

    public function login(Request $request) {
        
        $username = $request->get('username');
        $password = $request->get('password');
        
        $user = User::where('name', $username)->first();

        if (!empty($user))
        {
            if(Hash::check($password, $user->password))
            {
                // New session
                // Bandysiu tikrint ar prisijunges ar ne, jei ne tai duos tik basic route naudot :)
                session()->regenerate();
                $request->session()->put('name', $user->name);
                return redirect('/crud');
            }
            else 
            {
                return view('login');
            }
        }
        return view('login');
    }

    public function changepassword(Request $request)
    {
        if(!empty(session('name')))
        {
            $oldpass = $request->get('oldpass');
            $newpass1 = $request->get('newpass1');
            $newpass2 = $request->get('newpass2');

            // SESSION USER
            $username = session('name');

            $user = User::where('name', $username)->first();

            if(!empty($user))
            {
                if(Hash::check($oldpass, $user->password))
                {
                    if($newpass1 == $newpass2)
                    {
                        User::where('name', $username)->update(['password' => Hash::make($newpass1)]);
                        return redirect('/crud');
                    }
                    else
                    {
                        return view('/changepassword');
                    }
                }
                else 
                {
                    return redirect('/crud');
                }
            }
            return redirect('/crud');
        }
        return redirect('/');
    }

    public function createCategory(Request $request)
    {
        if(!empty(session('name')))
        {
            $category = $request->get('category');

            $exits = Category::where('category', $category)->first();
            if(empty($exits))
            {
                $newCat = new Category();
                $newCat->category = $category;
                $newCat->save();
            }
            return redirect('/crud');
        }
        return redirect('/');
    }

    public function logout()
    {
        session()->flush();
        return redirect('/');
    }
}

